package com.jkanetwork.st.calendariodefrases;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;


import java.util.ArrayList;
import java.util.List;

import static java.lang.String.valueOf;


/**
 * Created by kprkpr on 21/03/17.
 */

public class OptsActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opts);
        final Config conf = new Config(this);
        /* Do variables of objects */
        final Spinner spinnerHour = (Spinner) findViewById(R.id.spinnerHour);
        final Spinner spinnerMinute = (Spinner) findViewById(R.id.spinnerMinute);
        final CheckBox chkalarm = (CheckBox) findViewById(R.id.chk_alarm);
        final CheckBox chkpersistent = (CheckBox) findViewById(R.id.chk_persistent);
        /* Set texts and checks from configs */
        chkalarm.setChecked(conf.getNotifOn());
        chkpersistent.setChecked(conf.getPersistNotifOn());

        // Hour and minute elements create

        // Spinner Drop down elements
        List<Integer> Hours = new ArrayList<>();
        for (int x = 0; x <= 23; x++) {
            Hours.add(x);
        }
        List<Integer> Minutes = new ArrayList<>();
        for (int x = 0; x <= 59; x++) {
            Minutes.add(x);
        }
        // Creating adapter for spinner
        ArrayAdapter<Integer> hourAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Hours);
        ArrayAdapter<Integer> minuteAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Minutes);
        // Drop down layout style - list view with radio button
        hourAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        minuteAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinnerHour.setAdapter(hourAdapter);
        spinnerMinute.setAdapter(minuteAdapter);

        spinnerHour.setSelection(conf.getHour());
        spinnerMinute.setSelection(conf.getMinute());


        setEnabledParts();

         /* Disable options if notification is disabled */
        chkalarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setEnabledParts();
            }
        });
        /* For not set date if persistent is enabled */
        chkpersistent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setEnabledParts();
            }
        });


        Button btnsave = (Button) findViewById(R.id.btnSaveNotif);
        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer hourSave, minuteSave;

                hourSave = Integer.decode(String.valueOf(spinnerHour.getSelectedItemPosition()));

                minuteSave = Integer.decode(String.valueOf(spinnerMinute.getSelectedItemPosition()));

                conf.setHour(hourSave);
                conf.setMinute(minuteSave);
                //Log.d("EnabledNotif", String.valueOf(chkalarm.isChecked()));
                /* Enable or disable notification and persistent */
                conf.setNotifOn(chkalarm.isChecked());
                conf.setPersistNotifOn(chkpersistent.isChecked());

                /* Cancel notif when save changes first */
                NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                nm.cancel(1);

                Alarm alarm = new Alarm();
                alarm.putAlarm(OptsActivity.this);
                if (chkpersistent.isChecked() && chkalarm.isChecked()) {
                    OptsActivity.this.startService(new Intent(OptsActivity.this, NotificationHelper.class));
                }
            }
        });

    }
    public void setEnabledParts(){
        /* I have to set vars again because is out of onCreate */
        Spinner notifHour = findViewById(R.id.spinnerHour);
        Spinner notifMinute = findViewById(R.id.spinnerMinute);
        CheckBox chkalarm = findViewById(R.id.chk_alarm);
        CheckBox chkpersistent = findViewById(R.id.chk_persistent);
        if (chkalarm.isChecked()){
            chkpersistent.setEnabled(true);
                    /* This other checks is chkpersistent checked dependent */
            if (chkpersistent.isChecked()){
                notifHour.setEnabled(false);
                notifMinute.setEnabled(false);
            }else{
                notifHour.setEnabled(true);
                notifMinute.setEnabled(true);
            }
        }else{
            chkpersistent.setEnabled(false);
            notifHour.setEnabled(false);
            notifMinute.setEnabled(false);
        }
    }

}

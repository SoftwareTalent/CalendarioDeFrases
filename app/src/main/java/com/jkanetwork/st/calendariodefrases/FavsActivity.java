package com.jkanetwork.st.calendariodefrases;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by kprkpr on 21/03/17.
 * Help from: http://androidexample.com/Create_A_Simple_Listview_-_Android_Example/index.php?view=article_discription&aid=65
 */

public class FavsActivity extends AppCompatActivity {
    DatabaseHelper db = new DatabaseHelper(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favs);

        final ListView listFavs = (ListView) findViewById(R.id.listSentences);

        // Defined Array values to show in ListView

        //Get IDs of sentences that are favs, and create an array of same length
        final Integer[] IDFavs = db.getIDFavs();

        final String[] sentences = new String[IDFavs.length];
        final String[] authorNames = new String[IDFavs.length];
        //Follow all IDFavs recovering sentence
        if (IDFavs.length != 0) {
            for (int x = 0; x <= IDFavs.length - 1; x++) {
                sentences[x] = db.getFraseFromID(IDFavs[x]);
                authorNames[x] = db.getAutorFromID(IDFavs[x]);
            }
        }

        if (IDFavs.length != 0) {
            ArrayList<HashMap<String, String>> items = new ArrayList<>();
            HashMap<String, String> listItem;
            for (int x = 0; x <= IDFavs.length - 1; x++) {
                listItem = new HashMap<>();
                listItem.put("item", sentences[x]);
                listItem.put("subitem", authorNames[x]);
                items.add(listItem);
            }
            SimpleAdapter adapter = new SimpleAdapter(this, items, android.R.layout.simple_list_item_2, new String[]{"item", "subitem"}, new int[]{android.R.id.text1, android.R.id.text2});
            listFavs.setAdapter(adapter);
        }

        /* Register context menu */
        registerForContextMenu(listFavs);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.proverbmenu_del, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        ListView listFavs = (ListView) findViewById(R.id.listSentences);
        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        @SuppressWarnings("unchecked") /* Because next line has a type change without checking it */
        HashMap<String, String> obj = (HashMap<String,String>) listFavs.getItemAtPosition(info.position);

        switch (item.getItemId()) {
            case R.id.copy:
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setPrimaryClip(ClipData.newPlainText("Sentence", obj.get("item")+this.getString(R.string.sharefrom)));
                Toast.makeText(getApplicationContext(), "Copiado al portapapeles", Toast.LENGTH_LONG ).show();
                return true;
            case R.id.delfav:
                Integer[] IDFavs = db.getIDFavs();
                db.delFav(IDFavs[info.position]);
                /* Restart activity for refresh favs */
                finish();
                startActivity(getIntent());
                return true;
            case R.id.share:
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT,obj.get("item")+this.getString(R.string.sharefrom));
                startActivity(Intent.createChooser(intent, "Compartir con"));
                return true;
        }
        return false;
    }
}

package com.jkanetwork.st.calendariodefrases;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kprkpr on 21/03/17.
 */

public class Config {

    private final String SHARED_PREFS_FILE = "Prefs";
    private final String KEY_HOUR = "Hour";
    private final String KEY_MINUTE = "Minute";
    private final String KEY_NOTIFON = "Notify";
    private final String KEY_PERSISTNOTIFON = "PersistNotify";
    private final String KEY_FIRSTTIME = "FirstTime";

    private Context emContext;


    public Config(Context context){
        emContext = context;
    }


    private SharedPreferences getSettings(){
        return emContext.getSharedPreferences(SHARED_PREFS_FILE, 0);
    }

    Integer getHour(){
        return getSettings().getInt(KEY_HOUR, 8);
    }
    Integer getMinute(){
        return getSettings().getInt(KEY_MINUTE, 0);
    }
    Boolean getNotifOn(){
        return getSettings().getBoolean(KEY_NOTIFON, true);
    }
    Boolean getPersistNotifOn(){return getSettings().getBoolean(KEY_PERSISTNOTIFON, false);}
    Boolean getFirstTime(){return getSettings().getBoolean(KEY_FIRSTTIME, true);}


    void setHour(Integer hour){
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putInt(KEY_HOUR, hour );
        editor.apply();
    }
    void setMinute(Integer minute){
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putInt(KEY_MINUTE, minute );
        editor.apply();
    }

    void setNotifOn(Boolean notifon){
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putBoolean(KEY_NOTIFON, notifon );
        editor.apply();
    }

    void setPersistNotifOn(Boolean notifon){
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putBoolean(KEY_PERSISTNOTIFON, notifon );
        editor.apply();
    }

    void setFirstTime(Boolean firstTime){
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putBoolean(KEY_FIRSTTIME, firstTime );
        editor.apply();
    }

}

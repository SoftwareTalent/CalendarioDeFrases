package com.jkanetwork.st.calendariodefrases;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.marcohc.robotocalendar.RobotoCalendarView.RobotoCalendarListener;
import com.marcohc.robotocalendar.RobotoCalendarView;
import java.text.DateFormatSymbols;
import java.util.Calendar;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;



public class MainActivity extends AppCompatActivity implements RobotoCalendarListener {

    private RobotoCalendarView robotoCalendarView;
    DatabaseHelper db = new DatabaseHelper(this);
    Calendar cal = Calendar.getInstance();
    Integer IDFrase;
    final Config conf = new Config(this);

    /* Will use when move month back and forth, because library doesnt show the actual month.. */
    Integer calMonth = cal.get(Calendar.MONTH)+1; //From 1 to 12, and not from 0 to 11
    Integer calYear = cal.get(Calendar.YEAR);

    /* Texts making all activity used, and valued in onCreate */
    TextView txtfraseTitle;
    TextView txtfraseText;
    TextView txtfraseAutor;
    /* More objects of view */
    ImageButton btnFav;
    ImageButton btnShare;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.favorites:
                Intent newIF= new Intent(MainActivity.this,FavsActivity.class);
                startActivity(newIF);
                return true;
            case R.id.options:
                Intent newIO= new Intent(MainActivity.this,OptsActivity.class);
                startActivity(newIO);
                return true;
            case R.id.about:
                Intent newIA= new Intent(MainActivity.this,AboutOfActivity.class);
                startActivity(newIA);
                return true;
            case R.id.exit:
                moveTaskToBack(true); /* Returns to "desktop" */
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* First of all,before loading, see if its the first time using the app */
        if (conf.getFirstTime()){
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Bienvenido")
                    .setMessage("Bienvenidos a Calendario de Frases, donde cada día disfrutarás de una nueva cita celebre o de un nuevo refrán que podrás guardar o compartir con tus amigos.\nAdemás, podrás poner una notificación para que te avise de la frase del día a la hora que quieras, por ejemplo, al desperarte.\nRecuerda que no puedes ver frases de los días que no hayan pasado aún con la aplicación.\nEsperamos que la disfrutes.")
                    .setCancelable(false)
                    .setPositiveButton("Empecemos", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Nothing
                        }
                    }).show();
            conf.setFirstTime(false); //Only show first time
        }

        setContentView(R.layout.activity_main);
        txtfraseTitle = findViewById(R.id.txt_frasetitle);
        txtfraseText = findViewById(R.id.txt_frasetext);
        txtfraseAutor = findViewById(R.id.txt_fraseautor);
        btnFav = findViewById(R.id.btnFav);
        btnShare = findViewById(R.id.btnShare);

        /* Calendar basic settings */
        robotoCalendarView = (RobotoCalendarView) findViewById(R.id.calendarPicker);
        robotoCalendarView.setRobotoCalendarListener(this);
        robotoCalendarView.setShortWeekDays(false);
        robotoCalendarView.showDateTitle(true);

        /* Admob ads */
        MobileAds.initialize(getApplicationContext());
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);                                          //request a new ad



        /* Configure alarm */
        Alarm alarm = new Alarm();
        alarm.putAlarm(this);

        /* Register context menu */
        registerForContextMenu(txtfraseText);
    }

    @Override
    public void onResume(){
        super.onResume();
        /* Refresh calendar */
        cal = Calendar.getInstance();
        calMonth = cal.get(Calendar.MONTH)+1; //From 1 to 12, and not from 0 to 11
        calYear = cal.get(Calendar.YEAR);


        //Load today's sentence and set texts
        IDFrase = db.getIDFraseHoy();

        /* Refresh calendar, after load sentences for checks */
        robotoCalendarView.updateView();
        markDates();

        txtfraseText.setText(db.getFraseFromID(IDFrase));
        txtfraseAutor.setText(db.getAutorFromID(IDFrase));


        // ListView Item Click Listener
        txtfraseText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View viewIn) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setPrimaryClip(ClipData.newPlainText("Sentence", txtfraseText.getText()+MainActivity.this.getString(R.string.sharefrom)));
                Toast.makeText(getApplicationContext(), "Copiado al portapapeles", Toast.LENGTH_LONG ).show();
            }
        });
        changeFavImage(); //Fav image button

        // SetFavListener
        btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View viewIn) {
                if (db.getFav(IDFrase) == 1){
                    db.delFav(IDFrase);
                }else {
                    db.addFav(IDFrase);
                }
                changeFavImage();
            }
        });
        // ShareListener
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View viewIn) {
                String sentence = txtfraseText.getText().toString();
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, sentence+MainActivity.this.getString(R.string.sharefrom));
                startActivity(Intent.createChooser(intent, "Compartir con"));
            }
        });
    }

    private void changeFavImage() {
        if (db.getFav(IDFrase) == 1){
            btnFav.setBackgroundResource(R.drawable.star_on);
        }else {
            btnFav.setBackgroundResource(R.drawable.star_off);
        }
    }


    /* Close app on back button */
    @Override
    public void onBackPressed()
    {
        moveTaskToBack(true);
    }



    @Override
    public void onDayClick(Calendar daySelectedCalendar) {
        Integer month = daySelectedCalendar.get(Calendar.MONTH);
        Integer day = daySelectedCalendar.get(Calendar.DAY_OF_MONTH);
        Integer year = daySelectedCalendar.get(Calendar.YEAR);
        month=month+1; //Now months start with 1 and not 0
                /* Load sentence */
        IDFrase = db.getIDFraseDay(year, month, day);
        String frase = db.getFraseFromID(IDFrase);
        txtfraseText.setText(frase); //Change text
        txtfraseAutor.setText(db.getAutorFromID(IDFrase));
                /* Set Title before sentence */
        if (year != cal.get(Calendar.YEAR) || month != (cal.get(Calendar.MONTH)+1) || day != cal.get(Calendar.DAY_OF_MONTH)) {
            txtfraseTitle.setText("Frase del " + day + " de " + getMonth(month)); //Change title
        }else {
            txtfraseTitle.setText(R.string.txt_todaysentence); //Change title (today)
        }
        changeFavImage(); //For checking fav image
    }
    @Override
    public void onDayLongClick(Calendar daySelectedCalendar) {
        onDayClick(daySelectedCalendar);
    }


    /** It returns the month name */
    public String getMonth(int month) {
        String str = new DateFormatSymbols().getMonths()[month-1];
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    public void markDates() {
        robotoCalendarView.clearCalendar();

        Calendar time = Calendar.getInstance();
        String[] IDDays = db.getAllDaysYet();
        if (IDDays.length != 0) {
            for (int x = 0; x <= IDDays.length - 1; x++) {
                Integer year = Integer.parseInt(IDDays[x].substring(0, 4));
                Integer month = Integer.parseInt(IDDays[x].substring(4, 6));
                Integer day = Integer.parseInt(IDDays[x].substring(6, 8));
                if (year.equals(calYear) && month.equals(calMonth)) {
                    time.set(Calendar.DAY_OF_MONTH, day);
                    time.set(Calendar.MONTH, month-1);
                    robotoCalendarView.markCircleImage1(time);
                }
            }
        }
    }


    /* Overrides for month plus and minus */
    @Override
    public void onRightButtonClick() {
        calMonth += 1;
        if (calMonth > 12){
            calMonth = 1;
            calYear += 1;
        }
        markDates();
    }

    @Override
    public void onLeftButtonClick() {
        calMonth -= 1;
        if (calMonth < 1){
            calMonth = 12;
            calYear -= 1;
        }
        markDates();
    }

}
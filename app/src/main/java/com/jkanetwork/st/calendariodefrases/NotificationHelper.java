package com.jkanetwork.st.calendariodefrases;



import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;
import android.util.Log;


/**
 * Created by kprkpr on 20/03/17.
 */

public class NotificationHelper extends Service {
    private static final String TAG = "NotificationHelper";
    DatabaseHelper db = new DatabaseHelper(this);

    @Override
    public IBinder onBind(Intent i) {
        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {

        new Thread(new Runnable() {

            @Override
            public void run() {
                Log.d(TAG, "Service Started");
                // El servicio se finaliza a sí mismo cuando finaliza su
                // trabajo.
                final Config conf = new Config(NotificationHelper.this);
                try {
                    // Instanciamos e inicializamos nuestro manager.
                    NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                    nm.cancel(1); /* Cancel first */
                    if (conf.getNotifOn().equals(true)) { /* Only set things if notification is enabled */
                        Intent OpenIntent = new Intent(NotificationHelper.this, MainActivity.class);
                        PendingIntent notifIntent = PendingIntent.getActivity(NotificationHelper.this, 0, OpenIntent, PendingIntent.FLAG_IMMUTABLE);

                        CharSequence ticker = "Frase del día";
                        CharSequence contentTitle = "Frase de hoy:";
                        CharSequence contentText = db.getFraseFromID(db.getIDFraseHoy());
                        NotificationCompat.Builder notibuilder = new NotificationCompat.Builder(NotificationHelper.this,"notif")
                            .setContentIntent(notifIntent)
                            .setTicker(ticker)
                            .setContentTitle(contentTitle)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(contentText)) /* For big texts */
                            .setContentText(contentText) /* For when bigText cant be show */
                            .setLargeIcon(BitmapFactory.decodeResource(NotificationHelper.this.getResources(), R.mipmap.ic_launcher));
                            /* If lollipop, new white icon, if not, grey icon */
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
                                notibuilder.setSmallIcon(R.drawable.notifwhite);
                            } else{
                                notibuilder.setSmallIcon(R.drawable.notifgris);
                            }

                        Notification noti = notibuilder.build();
                        if (conf.getPersistNotifOn().equals(true)) {
                            noti.flags = Notification.FLAG_ONGOING_EVENT;
                        }
                        nm.notify(1, noti);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }).start();
        Log.d(TAG, "Service Finished");
        this.stopSelf();
        return super.onStartCommand(intent, flags, startId);
    }


}

# Codigo fuente de "Calendario De Frases"

Calendario de frases es una aplicacion para Android en la que todos los días tendrás una nueva frase o refrán en tu lugar de notificaciones

Con un calendario integrado, podrás ver frases anteriores, marcar como favoritos, o compartirlas con tus amigos

Descargalo en Google Play: https://play.google.com/store/apps/details?id=com.jkanetwork.st.calendariodefrases

## Problemas al compilar la aplicación

La aplicación puede dar problemas de compilación al faltar los 2 strings de la API de Admob (No subidas porque son propias, evitamos un mal uso de la API).
Para solucionarlo solo hay que quitar la parte de admob

## Licencia

Mire el fichero LICENSE.md para detalles, usamos licencia GPL